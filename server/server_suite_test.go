package main_test

import (
	"bdd/server/server"
	"os/exec"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/sclevine/agouti"

	"testing"
)

func TestServer(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Server Suite")
}

var agoutiDriver *agouti.WebDriver
var baseURL, body, url string
var page *agouti.Page

var _ = BeforeSuite(func() {
	exec.Command("killPort.sh")
	time.Sleep(5 * time.Second)

	baseURL = "http://localhost:8000/"
	go server.StartServer("8000")

	// Choose a WebDriver:

	//agoutiDriver = agouti.PhantomJS()
	agoutiDriver = agouti.ChromeDriver()

	Expect(agoutiDriver.Start()).To(Succeed())

})

var _ = AfterSuite(func() {
	Expect(agoutiDriver.Stop()).To(Succeed())
})
