package main_test

import (
	"fmt"
	"image/png"
	"log"

	"os"
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/sclevine/agouti/matchers"
)

var _ = Describe("Server", func() {
	delay := 0

	BeforeEach(func() {
		var err error
		page, err = agoutiDriver.NewPage()
		Expect(err).NotTo(HaveOccurred())
	})

	AfterEach(func() {
		Expect(page.Destroy()).To(Succeed())
	})

	It("Should display the home page when no path is entered", func() {

		Expect(page.Navigate(baseURL)).To(Succeed())
		Eventually(page).Should(HaveURL(baseURL))
		//body := page.Find(`body`)
		title, _ := page.Title()
		fmt.Printf("Title [%s] \n", title)
		Expect(title).To(Equal("Server Home Page"))
		Expect(page.Find(`h1`).Text()).To(Equal("Server Home Page"))

	})

	passTable := []struct {
		context string
		cmd     string
		symbol  string
		first   string
		second  string
		total   string
	}{
		{"should add two integers", "add", "+", "55", "32", "87"},
		{"should subtract integers", "subtract", "-", "54", "32", "22"},
		{"should subtract negative integers - FAIL NOT HANDLED", "subtract", "-", "-32", "10", "-42"},
		{"should multiply two integers", "multiply", "*", "5", "3", "15"},
		{"should multiply by zero results in zero", "multiply", "*", "5", "0", "0"},
		{"should IntegerDivide two integers", "divide", "/", "9", "4", "2"},
		{"should IntegerDivide smaller with larger results in zero", "divide", "/", "3", "4", "0"},
	}

	for _, row := range passTable {
		row := row

		It(row.context, SaveScreenshotOnFail(func() {
			testPassFixture(row.cmd, row.symbol, row.first, row.second, row.total, delay)
		}))
	}

	usageMsg := `Server Home Page
USAGE:
/add/x/y
/subtract/x/y
/multiply/x/y
/divide/x/y
wherexandyareintegers`

	fmt.Println(usageMsg)

	errorTable := []struct {
		desc string
		url  string
	}{
		{"should display usage information when no parameters are entered", ""},
		{"should display usage information when invalid command is entered", "adds/3/4"},
		{"should display usage information when missing first operand", "adds//4"},
		{"should display usage information when missing second operand", "adds/23/"},
		{"should display usage information when missing both operands", "add//"},
	}
	for _, row := range errorTable {
		row := row
		delay = 2
		It(row.desc, SaveScreenshotOnFail(func() {
			testErrorFixture(baseURL+row.url, usageMsg, delay)
		}))
	}

})

func testPassFixture(cmd string, symbol string, first string, second string, total string, delay int) {

	url = baseURL + cmd + "/" + first + "/" + second
	log.Println(url)

	Expect(page.Navigate(url)).To(Succeed())
	if delay > 0 {
		time.Sleep(1 * time.Second)
	}

	Expect(page.FindByID(`n1`).Text()).To(Equal(first))
	Expect(page.FindByID(`symbol`).Text()).To(Equal(symbol))
	Expect(page.FindByID(`n2`).Text()).To(Equal(second))
	Expect(page.FindByID(`result`).Text()).To(Equal(total))
}

func testErrorFixture(testURL string, expected string, delay int) {
	url = testURL
	Expect(page.Navigate(url)).To(Succeed())
	//Eventually(page).Should(HaveURL(url))
	if delay > 0 {
		time.Sleep(1 * time.Second)
	}

	html, _ := page.Find("body").Text()
	html = strings.Replace(html, " ", "", -1)
	expected = strings.Replace(expected, " ", "", -1)
	//fmt.Printf("HTML => [%s]\n", html)
	//fmt.Printf("Expected => [%s]\n", expected)
	Expect(html).To(Equal(expected))
}

func takeScreenshot() {

	fmt.Printf("url lenghth %d \n ", len(url))
	if len(url) == 0 {
		url = "no_url"
	}
	name := url + ".png"
	name = strings.Replace(name, "/", "_", -1)
	name = strings.Replace(name, "__", "_", -1)
	name = "screenshots/" + strings.Replace(name, ":", "", -1)
	fmt.Println("**** Screenshot : " + name)
	Expect(page.Screenshot(name)).To(Succeed())
	//defer os.Remove(name)
	file, _ := os.Open(name)
	_, err := png.Decode(file)
	Expect(err).NotTo(HaveOccurred())
}

func SaveScreenshotOnFail(body func()) func() {
	return func() {
		failures := InterceptGomegaFailures(body)
		if len(failures) > 0 {
			takeScreenshot()
		}
		Expect(failures).To(BeEmpty())
	}
}
