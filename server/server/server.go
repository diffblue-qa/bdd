package server

import (
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

func StartServer(port string) {
	var dir string

	flag.StringVar(&dir, "dir", "./static", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()

	r := mux.NewRouter()

	// This will serve files under http://localhost:8000/static/<filename>
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(dir))))

	r.HandleFunc("/", HomeHandler)
	r.HandleFunc("/add/{first:[0-9]+}/{second:[0-9]+}", AddHandler)
	r.HandleFunc("/subtract/{first:[0-9]+}/{second:[0-9]+}", SubtractHandler)
	r.HandleFunc("/multiply/{first:[0-9]+}/{second:[0-9]+}", MultiplyHandler)
	r.HandleFunc("/divide/{first:[0-9]+}/{second:[0-9]+}", IntDivideHandler)
	//r.HandleFunc("/", FloatDivideHandler)
	r.HandleFunc("/{anything:.*}", ErrorHandler)

	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:" + port,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	// Bind to a port and pass our router in
	log.Fatal(srv.ListenAndServe())
}

type Page struct {
	PageTitle string
	N1        string
	Symbol    string
	N2        string
	Equals    string
	Result    string
	Error     string
}

type Empty struct{}

var usage = "USAGE:\n/add/xy\n/subtract/x/y\n/multiply/x/y\n/divide/x/y\nwhere x and y are integers"

func HomeHandler(w http.ResponseWriter, request *http.Request) {

	data := Empty{}
	w.WriteHeader(http.StatusOK)
	//fmt.Fprintln(w, "Home Page ")
	tmpl, _ := template.ParseFiles("static/home.html")
	tmpl.Execute(w, data)

}

func ErrorHandler(w http.ResponseWriter, r *http.Request) {

	//	w.WriteHeader(http.StatusOK)
	//	fmt.Fprintln(w, "Usage Error \n\n")
	//	fmt.Fprintln(w, usage)
	tmpl, err := template.ParseFiles("page.html")

	if err == nil {
		fmt.Println(err)
	}

	data := Page{"Server Home Page", "", "", "", "", "", usage}

	//w.WriteHeader(http.StatusOK)
	tmpl.Execute(w, data)

}

func AddHandler(w http.ResponseWriter, r *http.Request) {
	handler(w, r, "add", "+")
}

func SubtractHandler(w http.ResponseWriter, r *http.Request) {
	handler(w, r, "subtract", "-")
}

func MultiplyHandler(w http.ResponseWriter, r *http.Request) {
	handler(w, r, "multiply", "-")
}

func IntDivideHandler(w http.ResponseWriter, r *http.Request) {
	handler(w, r, "divide", "/")
}

func handler(w http.ResponseWriter, r *http.Request, cmd string, symbol string) {

	var first, second, result, errorMsg string
	var n1, n2 int
	equals := "="

	vars := mux.Vars(r)

	fmt.Println("First : " + vars["first"])
	fmt.Println("Second : " + vars["second"])

	tmpl, err := template.ParseFiles("page.html")

	if err == nil {
		fmt.Println(err)
	}
	//fmt.Fprintf(w, "Add : %v +  %v\n", vars["first"], vars["second"])

	n1, _ = strconv.Atoi(vars["first"])
	n2, _ = strconv.Atoi(vars["second"])

	result = strconv.Itoa(n1 + n2)
	first = vars["first"]
	second = vars["second"]

	if cmd == "add" {
		result = strconv.Itoa(n1 + n2)
		symbol = "+"
	} else if cmd == "subtract" {
		result = strconv.Itoa(n1 - n2)
		symbol = "-"
	} else if cmd == "multiply" {
		result = strconv.Itoa(n1 * n2)
		symbol = "*"
	} else if cmd == "divide" {
		result = strconv.Itoa(n1 / n2)
		symbol = "/"
	}

	data := Page{"Server Home Page", first, symbol, second, equals, result, errorMsg}

	//w.WriteHeader(http.StatusOK)
	tmpl.Execute(w, data)
}
