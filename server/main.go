package main

import "fmt"
import "bdd/server/server"

func main() {

	port := "8000"
	fmt.Printf("http://localhost:%s", port)
	server.StartServer(port)

}
