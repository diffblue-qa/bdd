# Webserver Project


The main package has been split into sseveral files, so all need to be referenced

To start the server

```bash
./startServer.sh
```

To run the tests headless

```bash
go test
```

To run the tests with a visible browser

```bash
go test --headless=false
```
