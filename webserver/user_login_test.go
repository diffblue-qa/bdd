package main_test

import (
	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/sclevine/agouti/matchers"
)

//var errMsg = "Password must be minimum 8 characters with at least one UpperCase and Number"

var _ = Describe("UserLogin", func() {

	BeforeEach(func() {
		var err error
		page, err = agoutiDriver.NewPage()
		Expect(err).NotTo(HaveOccurred())
		if err != nil {
			l.Fatal("Failed to Open Page:", err)
		}
		URL = baseURL + "login"
	})

	AfterEach(func() {
		Expect(page.Destroy()).To(Succeed())
	})

	Context("When the Login  Page Loads correctly", func() {

		It("should display a login form and a usage message for call to /login", SaveScreenshotOnFail(func() {
			TEST = "load_login_page"
			l.Println("should display a login form and a usage message for call to /login")

			pageLoadHelper(URL)

			Eventually(page.Find("#username")).Should(BeFound())
			Expect(page.Find("#prompt")).To(HaveText("Please login!"))

			fldUname, _ := page.Find("#username").Attribute(`value`)
			fldPW, _ := page.Find("#password").Attribute(`value`)
			Expect(fldUname).To(Equal(""))
			Expect(fldPW).To(Equal(""))
			Expect(page.Find("#submitLogin").Active()).Should(Equal(false))
			Expect(page.Find("#usage").Text()).To(Equal(errMsg))
			//time.Sleep(2 * time.Second)
		}))
	})

	Context("When a User logs in with valid Credentials", func() {

		It("should navigate to the Profile Page after successful login", SaveScreenshotOnFail(func() {
			TEST = "valid_login"
			//URL = baseURL + "login"
			login(validUserName, validPassword)

			title, _ := page.Title()
			Expect(title).To(Equal("Profile"))

			h1, _ := page.Find(`h1`).Text()
			fmt.Printf("H1 : %s i\n", h1)

			//h1Text := "Profile : " + validUserName
			h1Text := "Profile"
			Expect(h1).To(Equal(h1Text))

			//time.Sleep(2 * time.Second)

			logout(validUserName)

			//time.Sleep(2 * time.Second)

		}))

		It("should be able to log out after successful login", SaveScreenshotOnFail(func() {
			TEST = "logout"
			pageLoadHelper(URL)
			login(validUserName, validPassword)

			// navigate back to login page
			pageLoadHelper(URL)

			title, _ := page.Title()
			Expect(title).To(Equal("Logout Page"))
			Expect(page.Find("#prompt")).To(HaveText("You are Logged in as " + validUserName))

			logout := page.Find("#submitLogout")
			Expect(logout.Attribute(`value`)).To(Equal("Logout"))
			Expect(page.Find("#logout_form").Submit()).To(Succeed())

			title2, _ := page.Title()
			Expect(title2).To(Equal("Login Page"))
		}))

	})

})
