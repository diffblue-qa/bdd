package main_test

import (
	"fmt"
	. "github.com/onsi/gomega"
	. "github.com/sclevine/agouti/matchers"
)

var errMsg = "Password must be minimum 8 characters with at least one UpperCase and Number"

func logout(userName string) {
	fmt.Println("logout called")
	pageLoadHelper(baseURL + "login")

	title, _ := page.Title()
	Expect(title).To(Equal("Logout Page"))
	Expect(page.Find("#prompt")).To(HaveText("You are Logged in as " + userName))

	logout := page.Find("#submitLogout")
	Expect(logout.Attribute(`value`)).To(Equal("Logout"))
	Expect(page.Find("#logout_form").Submit()).To(Succeed())

}

func login(userName, password string) {

	fmt.Printf("login called : username %s , password %s\n", userName, password)
	pageLoadHelper(baseURL + "login")

	funame := page.Find("#username")

	Eventually(funame).Should(BeFound())
	Expect(page.Find("#prompt")).To(HaveText("Please login!"))

	loginHelper(userName, password)

	Expect(page.Find("#login_form").Submit()).To(Succeed())
}

func loginHelper(userName, password string) {
	jsUpdateInputField("username", userName)
	jsUpdateInputField("password", password)
}

func saveProfile(data string) {

	postFile(baseURL+"profile", data)
	/*
		d := strings.Split(data, ",")
		name := d[0]
		day := d[1]
		month := d[2]
		year := d[3]
		interest := d[4]
		subject := d[5]
		photo := d[6]

		jsUpdateInputField("name", name)
		jsUpdateInputField("day", day)
		jsUpdateInputField("month", month)
		jsUpdateInputField("year", year)
		jsUpdateInputField("interest", interest)
		jsUpdateInputField("subject", subject)
		//	jsUpdateInputField("photo", photo)

		absFilePath, err := filepath.Abs("testdata/" + photo)
		if err != nil {
			fmt.Printf("Error %s", err)
		}
		fmt.Printf("AbsolutePath = %s \n", absFilePath)
		photoField := page.Find("#photo")
		photoField.SendKeys(absFilePath)
		//Expect(photoField.UploadFile("testdata/" + photo)).To(Succeed())
		time.Sleep(5 * time.Second)
		Expect(page.Find("#profile_form").Submit()).To(Succeed())
	*/

}
