package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func login(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("").ParseFiles("./templates/login.html", "./templates/base.html")
	if err != nil {
		panic(err)
	}

	pageTitle := "Login Page"
	data := Page{pageTitle, "", "", "", ErrMsg}

	//fmt.Println("method:", r.Method) //get request method

	if r.Method == "GET" {
		if loggedIn {
			data = Page{"Logout Page", loggedInUser, "", "", ""}
			t, err = template.New("").ParseFiles("./templates/logout.html", "./templates/base.html")
		}
		err = t.ExecuteTemplate(w, "base", data)
	} else {

		r.ParseForm()
		username := r.FormValue("username")
		password := r.FormValue("password")

		result := fmt.Sprintf("Username: %s ,  Password: %s", username, password)
		//fmt.Printf("Username: %s ,  Password: %s", username, password)

		data = Page{pageTitle, username, password, result, ErrMsg}

		if username == "admin" && password == "admin" {
			loggedIn = true
			loggedInUser = username
			http.Redirect(w, r, "/profile?name="+username, 303)
		} else {
			data = Page{pageTitle, username, password, result, "Invalid Credentials"}
			err = t.ExecuteTemplate(w, "base", data)
		}
	}

	if err != nil {
		panic(err)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {

	loggedIn = false
	loggedInUser = ""

	http.Redirect(w, r, "/login", 303)

}
