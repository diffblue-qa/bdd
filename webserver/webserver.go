package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

type Page struct {
	PageTitle string
	UserName  string
	Password  string
	Result    string
	Error     string
}

type ProfileType struct {
	PageTitle   string
	Name        string
	DOB         string
	Interest    string
	Subject     string
	Photo       string
	Description string
}

type ProfileFormType struct {
	PageTitle   string
	Status      string
	Name        string
	Day         string
	Month       string
	Year        string
	Interest    string
	Subject     string
	Photo       string
	Description string
}

var ProfileStore = "data/Profile.csv"
var ErrMsg = "Password must be minimum 8 characters with at least one UpperCase and Number"
var loggedIn = false
var loggedInUser string

func homePage(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("./templates/index.tpl")
	data := Page{"Webserver Home Page", "", "", "", ErrMsg}
	t.Execute(w, data)
}

func StartServer(port string) {
	fmt.Println("Server started on http://localhost:" + port)

	http.HandleFunc("/", homePage)

	// Serve images from assets subdirectory
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))
	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("./images"))))

	http.HandleFunc("/login", login)

	http.HandleFunc("/logout", logout)

	http.HandleFunc("/profile", profile)

	http.HandleFunc("/editProfile", editProfile)

	err := http.ListenAndServe(":"+port, nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func main() {

	port := "9090"
	StartServer(port)

}
