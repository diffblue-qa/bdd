package main_test

import (
	. "bdd/webserver"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"github.com/sclevine/agouti"

	"testing"
)

var (
	agoutiDriver                        *agouti.WebDriver
	page                                *agouti.Page
	baseURL, URL, TEST, body, reportDir string
	f                                   *os.File
	l                                   *log.Logger
	err                                 error
	headless                            string
)
var validUserName = "admin"
var validPassword = "admin"
var adminProfile0 = ",,,,,,,"
var adminProfile1 = "admin,12,10,1981,Football,Maths,Dr_Strange.png,Dr. Strange"
var adminProfile2 = "admin,11,08,1977,Rugby,Economics,,Keep Original Photo Test"
var adminProfile3 = "hacker,07,05,1979,Golf,French Literature,diffie.png,El33t H@xor"

func init() {
	flag.StringVar(&baseURL, "baseURL", "http://localhost:9090/", "baseURL is the base URL of the site under test")
	flag.StringVar(&reportDir, "reportDir", "logs", "reportDir is the target location of reports and screenshots")
	flag.StringVar(&headless, "headless", "true", "Set chrome to run headless , false to run in browser")

}

func TestWebserver(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("report.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "Webserver Suite", []Reporter{junitReporter})
}

var _ = BeforeSuite(func() {

	if _, err := os.Stat(reportDir); os.IsNotExist(err) {
		os.Mkdir(reportDir, os.ModePerm)
	}

	f, err = os.OpenFile(reportDir+"/testlogfile", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		l.Fatalf("error opening file: %v", err)
	}
	//defer f.Close()
	l = log.New(f, "", 0)

	// Delete Profile
	err := os.Remove("./" + ProfileStore)
	if err != nil {
		l.Printf("error deleting  file: %s  %v", ProfileStore, err)
	}
	// Delete Uploaded Image Files
	err = os.Remove("./assets/diffie.png")
	err = os.Remove("./assets/Dr_Strange.png")

	go StartServer("9090")
	l.Printf("Starting Server on Port 9090")
	// Choose a WebDriver:

	if headless == "false" {
		agoutiDriver = agouti.ChromeDriver()
		//agoutiDriver = agouti.PhantomJS()
		// agoutiDriver = agouti.Selenium()
	} else {
		agoutiDriver = agouti.ChromeDriver(
			agouti.ChromeOptions("args", []string{
				"--no-sandbox",
				"--headless",
				"--disable-gpu",
			}),
		)
	}

	//fmt.Printf("f: %T \n", f)
	//fmt.Printf("l : %T \n", l)

	resp, err := http.Get(baseURL)
	if err != nil {
		l.Fatal(err)
	}

	l.Println("HTTP Response Status:", resp.StatusCode, http.StatusText(resp.StatusCode))

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		fmt.Printf("%s HTTP Status is in the 2xx range\n", baseURL)
	} else {
		fmt.Println("Argh! Broken")
	}

	Expect(agoutiDriver.Start()).To(Succeed())

})

var _ = AfterSuite(func() {
	Expect(agoutiDriver.Stop()).To(Succeed())
})
