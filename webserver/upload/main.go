package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
)

func postFile(targetUrl string, data string) error {
	bodyBuf := &bytes.Buffer{}
	w := multipart.NewWriter(bodyBuf)

	d := strings.Split(data, ",")
	dname := d[0]
	dday := d[1]
	dmonth := d[2]
	dyear := d[3]
	dinterest := d[4]
	dsubject := d[5]
	photo := ""
	if len(d[6]) > 0 {
		photo = "testdata/" + d[6]
	}
	fmt.Println("Photo : " + photo)

	name, err := w.CreateFormField("name")
	if err != nil {
		return err
	}
	name.Write([]byte(dname))

	day, _ := w.CreateFormField("day")
	day.Write([]byte(dday))

	month, _ := w.CreateFormField("month")
	month.Write([]byte(dmonth))

	year, _ := w.CreateFormField("year")
	year.Write([]byte(dyear))

	interest, _ := w.CreateFormField("interest")
	interest.Write([]byte(dinterest))

	subject, _ := w.CreateFormField("subject")
	subject.Write([]byte(dsubject))

	if len(photo) > 0 {
		// this step is very important
		fileWriter, err := w.CreateFormFile("photo", photo)
		if err != nil {
			fmt.Println("error writing to buffer")
			return err
		}
		// open file handle
		fh, err := os.Open(photo)
		if err != nil {
			fmt.Println("error opening file")
			return err
		}
		defer fh.Close()

		//iocopy
		_, err = io.Copy(fileWriter, fh)
		if err != nil {
			return err
		}
	}

	contentType := w.FormDataContentType()
	w.Close()

	resp, err := http.Post(targetUrl, contentType, bodyBuf)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	resp_body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	fmt.Println(resp.Status)
	fmt.Println(string(resp_body))
	return nil
}

// sample usage
func main() {
	target_url := "http://localhost:9090/profile"
	//filename := "../testdata/Dr_Strange.png"
	//data := "admin,12,10,1981,Football,Maths,falcon.jpg"
	//data := "admin,11,03,1977,Rugby,French,diffie.png"
	//data := "admin,11,03,1977,Rugby,French,"
	data := "hacker,07,04,1967,Tennis,French,"
	postFile(target_url, data)
}
