<!DOCTYPE HTML>
<html>

<head>
  <title>{{.PageTitle}}</title>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div style="background-color:#000066;color:white;padding-left:50px;padding-top:10px;padding-bottom:10px;">
		<h1>{{.PageTitle}}</h1>
        </div>
      </div>
      <div id="menubar" style="background-color:#e6f7ff;color:white;padding-left:50px;padding-top:2px;padding-bottom:2px;">
        <ul id="menu" style="display: inline; list-style-type: none;margin: 0;padding: 0 ; ">
          <li style="display: inline;" class="selected"><a href="index.html">Home</a></li>
          <li style="display: inline;" ><a href="/login">Login</a></li>
          <li style="display: inline;"><a href="/profile">Profile</a></li>
        </ul>
      </div>
    </div>

    <div id="site_content">
      <div class="sidebar">
	      <!-- Sidebar -->
      </div>
      <div id="content" style="height:400px;border-color:#000066;border-style:solid; padding-left:50px;padding-top:20px;padding-bottom:50px;">
	      <!-- Content -->
	      <p id='prompt'>Please login!</p>

    <form id='login_form' action="/login" method="post">
        <table>
        <tr>
        <td>Username:</td>
                        <td><input id='username' type="text" name="username" value="{{.UserName}}">
            </td>
                </tr><tr>
            <td>Password:</td>
                        <td><input id='password' type="password" name="password" value="{{.Password}}">
            </td>
            </tr>
            <td></td>
            <td><input id='submitLogin' type="submit" value="Login">
        </td>
    </tr>
</table>
</form>

<div id='usage' style="color:red">
{{.Error}}
</div>

      </div>
    </div>

    <div id="content_footer"></div>

    <div id="footer" style="background-color:#000066;color:white;padding-left:50px;padding-top:5px;padding-bottom:5px;">
	    &copy; Powered by Diffblue 
    </div>
  </div>
</body>
</html>
