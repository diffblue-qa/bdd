<!DOCTYPE HTML>
<html>

<head>
  <title>Profile</title>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div style="background-color:#000066;color:white;padding-left:50px;padding-top:10px;padding-bottom:10px;">
		<h1>Profile : {{.Name}}</h1>
        </div>
      </div>
      <div id="menubar" style="background-color:#e6f7ff;color:white;padding-left:50px;padding-top:2px;padding-bottom:2px;">
        <ul id="menu" style="display: inline; list-style-type: none;margin: 0;padding: 0 ; ">
          <li style="display: inline;" class="selected"><a href="index.html">Home</a></li>
          <li style="display: inline;" ><a href="/login">Login</a></li>
          <li style="display: inline;"><a href="/profile">Profile</a></li>
        </ul>
      </div>
    </div>

    <div id="site_content">
      <div class="sidebar">
	      <!-- Sidebar -->
      </div>
      <div id="content" style="height:400px;border-color:#000066;border-style:solid; padding-left:50px;padding-top:20px;padding-bottom:50px;">
	      <!-- Content -->

		<table>
		<tr>
		<td>Name</td><td id="name">{{.Name}}</td>
		 </tr><tr>
		<td>DOB</td><td id="dob">{{.DOB}}</td>
		 </tr><tr>
		<td>Interests</td><td id="interest">{{.Interest}}</td>
		 </tr><tr>
		<td>Subject</td><td id="subject">{{.Subject}}</td>
		 </tr><tr>
		<td>Photo</td><td><img id="photo" src="/assets/{{.Photo}}" height="100px"></td>
		 </tr><tr>
		 </table>

<p>&nbsp;</p>
<p><a id="editLink"  href="/editProfile?name={{.Name}}">Edit Profile</a></p>

<div id='usage' style="color:red">
</div>

      </div>
    </div>

    <div id="content_footer"></div>

    <div id="footer" style="background-color:#000066;color:white;padding-left:50px;padding-top:5px;padding-bottom:5px;">
	    &copy; Powered by Diffblue 
    </div>
  </div>
</body>
</html>
