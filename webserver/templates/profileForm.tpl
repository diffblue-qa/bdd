<!DOCTYPE HTML>
<html>

<head>
  <title>Profile - Edit</title>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div style="background-color:#000066;color:white;padding-left:50px;padding-top:10px;padding-bottom:10px;">
		<h1>Profile - {{.Name}}</h1>
        </div>
      </div>
      <div id="menubar" style="background-color:#e6f7ff;color:white;padding-left:50px;padding-top:2px;padding-bottom:2px;">
        <ul id="menu" style="display: inline; list-style-type: none;margin: 0;padding: 0 ; ">
          <li style="display: inline;" class="selected"><a href="index.html">Home</a></li>
          <li style="display: inline;" ><a href="/login">Login</a></li>
          <li style="display: inline;"><a href="/profile">Profile</a></li>
        </ul>
      </div>
    </div>

    <div id="site_content">
      <div class="sidebar">
	      <!-- Sidebar -->
      </div>
      <div id="content" style="height:400px;border-color:#000066;border-style:solid; padding-left:50px;padding-top:20px;padding-bottom:50px;">
	      <!-- Content -->

        <form id='profile_form' action="/profile" method="post" enctype="multipart/form-data">
	<input type="hidden" name="status" value="{{.Status}}">
	<input id="name" type="hidden" name="name" value="{{.Name}}">
        <table>
        <tr>
        <td>
            Name:
            </td>
            <td>{{.Name}}
            </td>
            </tr>
            <tr>
            <td>
            DOB: </td>
            <td><input id='day' type="text" name="day" size="2" value="{{.Day}}">/<input id='month' type="text" name="month" size="2" value="{{.Month}}">/<input id='year' type="text" name="year" size="4" value="{{.Year}}">
            </td>
            </tr>
            <tr>
            <td>Favourite Sports </td>
            <td><input id='interest' type="text" name="interest" value="{{.Interest}}">
    </td>
    </tr>
    <tr>
        <td>Subject:</td>
            <td><input id='subject' type="text" name="subject" value="{{.Subject}}">
        </td>
    </tr>
    <tr>
        <td>Photo</td>
        <td>
	<img src="/assets/{{.Photo}}" height="100px">
           <input id='photo' type="file" id="photo" name="photo" size="40">
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <input id='profileSubmit' type="submit" value="Go">
        </td>
    </tr>
    </table>
        </form>


<div id='usage' style="color:red">
{{.Error}}
</div>

      </div>
    </div>

    <div id="content_footer"></div>

    <div id="footer" style="background-color:#000066;color:white;padding-left:50px;padding-top:5px;padding-bottom:5px;">
	    &copy; Powered by Diffblue 
    </div>
  </div>
</body>
</html>
