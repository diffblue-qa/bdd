package main_test

import (
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/sclevine/agouti/matchers"
)

var _ = Describe("UserLogin", func() {

	BeforeEach(func() {
		var err error
		page, err = agoutiDriver.NewPage()
		Expect(err).NotTo(HaveOccurred())
		if err != nil {
			l.Fatal("Failed to Open Page:", err)
		}
		URL = baseURL + "login"
	})

	AfterEach(func() {
		Expect(page.Destroy()).To(Succeed())
	})

	failTable := []struct {
		context  string
		username string
		password string
	}{
		{"should fail with empty username and password", "", ""},
		{"should fail with valid username and empty password", "admin", ""},
		{"should fail with empty username and valid password", "", "admin"},
		{"should fail with valid username and invalid password", "admin", "pasword"},
		{"should fail with invalid username and invalid password", "username", "pasword"},
	}

	for _, row := range failTable {
		row := row

		It(row.context, SaveScreenshotOnFail(func() {
			testLoginFail(row.username, row.password)
		}))
	}

})

func testLoginFail(userName, password string) {

	pageLoadHelper(URL)

	funame := page.Find("#username")
	Eventually(funame).Should(BeFound())
	Expect(page.Find("#prompt")).To(HaveText("Please login!"))

	loginHelper(userName, password)

	tfuname, _ := funame.Attribute(`value`)
	fmt.Println("Username : " + tfuname)

	//time.Sleep(2 * time.Second)
	Expect(page.Find("#login_form").Submit()).To(Succeed())

	bruname := GetInputFieldValue("#username")
	brpw := GetInputFieldValue("#password")
	//fmt.Printf("uname: %s  pw: %s \n", bruname, brpw)

	Expect(bruname).To(Equal(userName))
	Expect(brpw).To(Equal(password))

	msg, _ := page.Find("#usage").Text()
	//fmt.Printf("Message => %s \n", msg)
	Expect(msg).To(Equal("Invalid Credentials"))

}
