package main_test

import (
	//"fmt"
	"fmt"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("UserProfile", func() {

	BeforeEach(func() {
		var err error
		page, err = agoutiDriver.NewPage()
		Expect(err).NotTo(HaveOccurred())
		if err != nil {
			l.Fatal("Failed to Open Page:", err)
		}

		URL = baseURL + "login"
		pageLoadHelper(URL)
		login(validUserName, validPassword)
	})

	AfterEach(func() {
		logout(validUserName)
		Expect(page.Destroy()).To(Succeed())
	})

	Context("When the User Profile has not been saved", func() {

		It("should display Empty Profile Form ", SaveScreenshotOnFail(func() {
			TEST = "empty_user_profile"
			title, _ := page.Title()
			Expect(title).To(Equal("Profile"))

			h1, _ := page.Find(`h1`).Text()
			h1Text := "Profile"
			Expect(h1).To(Equal(h1Text))

			Expect(page.Find("#dob").Text()).To(Equal(""))
			Expect(page.Find("#interest").Text()).To(Equal(""))
			Expect(page.Find("#subject").Text()).To(Equal(""))

			img := page.Find("#photo")
			imgsrc, _ := img.Attribute(`src`)
			naturalHeight, _ := img.Attribute(`naturalHeight`)
			Expect(imgsrc).Should(HaveSuffix(`nophoto.jpg`))
			Expect(naturalHeight).To(Equal(`251`))

		}))
	})

	Context("When the Edit  User Profile Link is clicked ", func() {

		It("Displays the Profile Entry Form", SaveScreenshotOnFail(func() {
			TEST = "load_profile_form"

			link := page.Find("#editLink")
			Expect(link.Click()).To(Succeed())

			title, _ := page.Title()
			Expect(title).To(Equal("EditProfile"))
			Expect(page.Find(`h1`).Text()).To(Equal("EditProfile"))
		}))
	})
	Context("When the Profile Form is saved ", func() {

		It("should display the no photo image when no data is entered", SaveScreenshotOnFail(func() {
			TEST = "Enter_no_profile_data"
			Expect(page.Find("#editLink").Click()).To(Succeed())
			saveProfile(adminProfile0)
			checkProfile(adminProfile0, "nophoto.jpg")
		}))
		It("should display the entered data on the profile page", SaveScreenshotOnFail(func() {
			TEST = "Enter_profile_data"
			Expect(page.Find("#editLink").Click()).To(Succeed())
			saveProfile(adminProfile1)
			checkProfile(adminProfile1, "")
		}))

		It("should keep the original photo if no photo is uploaded", SaveScreenshotOnFail(func() {
			TEST = "Keep_profile_photo"

			img := page.Find("#photo")
			originalimg, _ := img.Attribute(`src`)
			fmt.Printf("Original Image : %s \n", originalimg)
			Expect(page.Find("#editLink").Click()).To(Succeed())
			saveProfile(adminProfile2)
			// Deliberate Error to show capture screenshot
			//checkProfile(adminProfile2, originalimg)
			checkProfile(adminProfile2, "error.png")
		}))

		It("should not change the name of the logged in user", SaveScreenshotOnFail(func() {
			TEST = "Keep_profile_name"

			//img := page.Find("#photo")
			//originalimg, _ := img.Attribute(`src`)
			Expect(page.Find("#editLink").Click()).To(Succeed())
			saveProfile(adminProfile3)
			checkProfile(adminProfile3, "")
		}))

	})

})

func checkProfile(profile string, originalimg string) {

	d := strings.Split(profile, ",")
	//name := d[0]  // User name must be that of logged in user
	dob := d[1] + "-" + d[2] + "-" + d[3]
	interest := d[4]
	subject := d[5]
	photo := d[6]
	//	description := d[7]

	pageLoadHelper(baseURL + "profile")
	//sleep(5)

	title2, _ := page.Title()
	Expect(title2).To(Equal("Profile"))

	Expect(page.Find("#name").Text()).To(Equal(validUserName))
	Expect(page.Find("#dob").Text()).To(Equal(dob))
	Expect(page.Find("#interest").Text()).To(Equal(interest))
	Expect(page.Find("#subject").Text()).To(Equal(subject))
	//	Expect(page.Find("#description").Text()).To(Equal(description))

	img := page.Find("#photo")
	imgsrc, _ := img.Attribute(`src`)
	if len(photo) == 0 {
		Expect(imgsrc).Should(HaveSuffix(originalimg))
	} else {
		Expect(imgsrc).Should(HaveSuffix(photo))
	}
}
