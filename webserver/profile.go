package main

import (
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func profile(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("").ParseFiles("./templates/profile.html", "./templates/base.html")
	if err != nil {
		panic(err)
	}

	data := ProfileType{"Profile", "", "", "", "", "", ""}

	fmt.Printf("[profile] Method is %s ", r.Method)

	if r.Method == "GET" {

		//username := r.URL.Query().Get("name")
		username := loggedInUser
		interest := ""
		subject := ""
		dob := ""
		photo := "nophoto.jpg"
		description := ""

		profile, exists := getProfile(username)
		if exists {
			interest = profile.Interest
			subject = profile.Subject
			dob = profile.DOB
			photo = profile.Photo
			description = profile.Description
		}
		t, err := template.New("").ParseFiles("./templates/profile.html", "./templates/base.html")
		if err != nil {
			panic(err)
		}

		data := ProfileType{"Profile", username, dob, interest, subject, photo, description}
		t.ExecuteTemplate(w, "base", data)

	} else {

		photoName := "nophoto.jpg"
		r.ParseMultipartForm(32 << 20)

		fmt.Printf("Form Status : %s \n", r.FormValue("status"))

		file, handler, err := r.FormFile("photo")
		if err != nil {
			fmt.Printf("No photo uploaded for %s", loggedInUser)
			profile, exists := getProfile(loggedInUser)
			if exists {
				photoName = profile.Photo
			}
			fmt.Printf("No Upload -> keep Existing photoName : %s", photoName)
		} else {

			if len(handler.Filename) > 0 {
				photoName = handler.Filename
				// remove filepath if it exists
				p := strings.Split(photoName, "/")
				if len(p) > 1 {
					photoName = p[len(p)-1]
				}
				fmt.Printf("Writing  ./assets/%s \n", photoName)
				f, err := os.OpenFile("./assets/"+photoName, os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					fmt.Printf("Cannot write to file : %s\n ", err)
					return
				}
				defer f.Close()
				io.Copy(f, file)
			} else {
				photoName = "nophoto.jpg"
			}

		}

		fmt.Printf("PhotoName : %s\n", photoName)

		dob := r.FormValue("day") + "-" + r.FormValue("month") + "-" + r.FormValue("year")
		fmt.Printf("DOB : %s %s %s %s \n", dob, r.FormValue("interest"), r.FormValue("subject"), loggedInUser)

		data = ProfileType{"Profile", loggedInUser, dob, r.FormValue("interest"), r.FormValue("subject"), photoName, r.FormValue("description")}

		saveProfile(loggedInUser, dob, r.FormValue("interest"), r.FormValue("subject"), photoName, r.FormValue("description"))

		t.ExecuteTemplate(w, "base", data)

	}

}

func editProfile(w http.ResponseWriter, r *http.Request) {

	//username := r.URL.Query().Get("name")
	username := loggedInUser

	if len(username) > 0 {
		t, err := template.New("").ParseFiles("./templates/profileForm.html", "./templates/base.html")
		if err != nil {
			panic(err)
		}

		data := ProfileFormType{"EditProfile", "create", username, "", "", "", "", "", "nophoto.jpg", ""}
		profile, exists := getProfile(username)

		if exists {
			interest := profile.Interest
			subject := profile.Subject
			dob := profile.DOB
			fmt.Printf("[editProfile] DOB = %s \n", dob)
			d := strings.Split(dob, "-")
			photo := profile.Photo
			description := profile.Description
			data = ProfileFormType{"EditProfile", "update", username, d[0], d[1], d[2], interest, subject, photo, description}
		}
		t.ExecuteTemplate(w, "base", data)
	} else {
		http.Redirect(w, r, "/login", 303)
	}
}

func saveProfile(name, dob, interest, subject, photo, description string) {
	if _, err := os.Stat(ProfileStore); os.IsNotExist(err) {
		os.Mkdir(ProfileStore, os.ModePerm)
	}

	// open input file for writing
	f, err := os.OpenFile(ProfileStore, os.O_RDWR, 0660)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	contents := fmt.Sprintf("%s,%s,%s,%s,%s,%s\n", name, dob, interest, subject, photo, description)
	fmt.Println("[saveProfile] " + contents)

	if _, err = f.WriteString(contents); err != nil {
		panic(err)
	}

}

func getProfile(name string) (ProfileType, bool) {
	exists := false
	profile := ProfileType{"Profile", "", "", "", "", "", ""}
	// Create empty file if the file does not exist
	if _, err := os.Stat(ProfileStore); os.IsNotExist(err) {
		fmt.Println("File does not exist")
		os.OpenFile(ProfileStore, os.O_RDONLY|os.O_CREATE, 0666)
	}

	file, err := ioutil.ReadFile(ProfileStore)
	if err != nil {
		log.Fatal(err)
	} else {

		if len(name) > 0 {
			strFile := string(file)
			// if the file is not empty
			if len(strFile) > 0 {
				lines := strings.Split(strFile, "\n")

				for line := range lines {
					fmt.Println("Profile : " + lines[0])
					data := strings.Split(lines[line], ",")
					if data[0] == name {
						profile = ProfileType{"Profile", data[0], data[1], data[2], data[3], data[4], data[5]}
						exists = true
						break
					}
				}
			}
		}
	}
	fmt.Printf("[getProfile] exists = %t \n", exists)
	return profile, exists
}
