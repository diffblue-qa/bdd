package main_test

import (
	"fmt"
	"image/png"
	"net/http"
	"os"
	"strings"
	"time"

	. "github.com/onsi/gomega"
	. "github.com/sclevine/agouti/matchers"
)

func pageLoadHelper(URL string) {
	fmt.Printf("Page Load Helper : %s \n", URL)
	resp, err := http.Get(URL)
	if err != nil {
		l.Fatal(err)
	}

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		fmt.Printf("%s HTTP Response Status: %d %s\n", URL, resp.StatusCode, http.StatusText(resp.StatusCode))
	} else {
		l.Fatal("Invalid Status Code : " + fmt.Sprintf(" %s : %d", URL, resp.StatusCode))
	}

	Expect(page.Navigate(URL)).To(Succeed())
	Eventually(page).Should(HaveURL(URL))

}

func jsUpdateInputField(id, value string) {
	fmt.Printf("Updating Field %s with data %s\n", id, value)
	jsName := "document.getElementById(\"" + id + "\").value=\"" + value + "\";"
	page.RunScript(jsName, nil, nil)
}

func GetInputFieldValue(id string) string {
	value, _ := page.Find(id).Attribute(`value`)
	return value
}

func SaveScreenshotOnFail(body func()) func() {
	return func() {
		failures := InterceptGomegaFailures(body)
		if len(failures) > 0 {
			TakeScreenshot()
		}
		Expect(failures).To(BeEmpty())
	}
}

func TakeScreenshot() {

	fmt.Printf("URL length %d \n ", len(URL))
	if len(URL) == 0 {
		URL = "no_URL"
	}
	name := URL + TEST + ".png"
	name = strings.Replace(name, "/", "_", -1)
	name = strings.Replace(name, "__", "_", -1)
	name = reportDir + "/" + strings.Replace(name, ":", "", -1)
	fmt.Println("** Saving  Screenshot : " + name)
	Expect(page.Screenshot(name)).To(Succeed())
	file, _ := os.Open(name)
	_, err := png.Decode(file)
	Expect(err).NotTo(HaveOccurred())
}

func sleep(secs time.Duration) {
	time.Sleep(secs * time.Second)
}
