package triangle_test

import (
	. "bdd/triangletest"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Triangle", func() {

	It("Should return an error if any side is negative ", func() {

		result, err := TriangleTester(-4, -4, -4)

		Expect(err).To(HaveOccurred())
		Expect(result).To(Equal(NotATriangle))
	})

	It("Should return an error if any side is zero ", func() {

		result, err := TriangleTester(0, 0, 0)

		Expect(err).To(HaveOccurred())
		Expect(result).To(Equal(NotATriangle))
	})

	It("Should return an error if one side is larger than the sum of the other 2 sides", func() {

		result, err := TriangleTester(3, 4, 10)

		Expect(err).To(HaveOccurred())
		Expect(result).To(Equal(NotATriangle))
	})

	It("Should return an error if the sum of the shorter two sides is equal to the third side", func() {
		result, err := TriangleTester(10, 20, 30)

		Expect(err).To(HaveOccurred())
		Expect(result).To(Equal(NotATriangle))
	})

	It("Should return an Equilateral triangle if all sides are equal, but not zero or negative", func() {
		result, err := TriangleTester(10, 10, 10)

		Expect(err).NotTo(HaveOccurred())
		Expect(result).To(Equal(Equilateral))
	})

	It("Should return an RightAngled triangle if the sum of the squares of 2 sides equals the square of the hypotenuse", func() {
		result, err := TriangleTester(3, 4, 5)

		Expect(err).NotTo(HaveOccurred())
		Expect(result).To(Equal(RightAngled))
	})

	It("Should return an Isosceles triangle if two sides are equal", func() {
		result, err := TriangleTester(20, 4, 20)

		Expect(err).NotTo(HaveOccurred())
		Expect(result).To(Equal(Isosceles))
	})

	It("Should return a scalene triangle f no sides are equal", func() {
		result, err := TriangleTester(10, 21, 30)

		Expect(err).NotTo(HaveOccurred())
		Expect(result).To(Equal(Scalene))
	})
})
