package triangle

import (
	"errors"
	"sort"
)

const (
	NotATriangle TriangleType = iota
	Equilateral
	Isosceles
	Scalene
	RightAngled
)

type TriangleType int

func TriangleTester(a, b, c int) (TriangleType, error) {

	sides := []int{a, b, c}

	// order the sides in ascending order
	sort.Ints(sides)

	// The length of the 2 shorter sides must be greater than the longest side
	// Sides cannot be of zero or negative length
	if sides[0]+sides[1] <= sides[2] ||
		sides[0] <= 0 {
		return NotATriangle, errors.New("Not a triangle")
	}

	// All 3 sides are equal
	if sides[0] == sides[1] && sides[1] == sides[2] {
		return Equilateral, nil
	}

	// Pythagoras' Theorem
	if square(sides[0])+square(sides[1]) == square(sides[2]) {
		return RightAngled, nil
	}

	// 2 sides are equal
	if sides[0] == sides[1] || sides[1] == sides[2] {
		return Isosceles, nil
	}

	// All other cases are scalene - valid triangle with all sides unequal
	return Scalene, nil
}

func square(a int) int {
	return a * a
}
