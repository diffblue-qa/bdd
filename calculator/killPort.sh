#!/bin/sh

echo "Kill Port :8000" 
kill -9 $(lsof -i TCP:8000 | grep LISTEN | awk '{print $2}')
