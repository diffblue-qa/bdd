package main_test

import (
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/sclevine/agouti"
	. "github.com/sclevine/agouti/matchers"
)

var _ = Describe("Api", func() {
	var page *agouti.Page
	baseUrl := "http://localhost:8000/"
	delay := 0

	BeforeEach(func() {

		var err error
		page, err = agoutiDriver.NewPage()
		Expect(err).NotTo(HaveOccurred())
	})

	AfterEach(func() {
		Expect(page.Destroy()).To(Succeed())
	})

	It("should add two integers", func() {

		cmd := "add"
		symbol := "+"
		first := "54"
		second := "32"
		total := "86"
		testFixture(page, cmd, symbol, first, second, total, delay)
	})

	It("should subtract two integers", func() {

		cmd := "subtract"
		symbol := "-"
		first := "54"
		second := "32"
		total := "22"
		testFixture(page, cmd, symbol, first, second, total, delay)
	})

	It("should multiply two integers", func() {

		cmd := "multiply"
		symbol := "*"
		first := "5"
		second := "3"
		total := "15"
		testFixture(page, cmd, symbol, first, second, total, delay)
	})

	It("should divide two integers", func() {

		cmd := "divide"
		symbol := "/"
		first := "15"
		second := "3"
		total := "5"
		testFixture(page, cmd, symbol, first, second, total, delay)
	})

	It("should display usage information", func() {

		usageMsg := `Calculator
USAGE:
/add/x/y
/subtract/x/y
/multiply/x/y
/divide/x/y
wherexandyareintegers

`
		usage := strings.Replace(usageMsg, " ", "", -1)
		url := baseUrl
		testError(page, url, usage)
	})

})

func testFixture(page *agouti.Page, cmd string, symbol string, first string, second string, total string, delay int) {

	baseUrl := "http://localhost:8000/"
	url := baseUrl + cmd + "/" + first + "/" + second
	Expect(page.Navigate(url)).To(Succeed())
	if delay > 0 {
		time.Sleep(1 * time.Second)
	}

	Expect(page.FindByID(`n1`).Text()).To(Equal(first))
	Expect(page.FindByID(`symbol`).Text()).To(Equal(symbol))
	Expect(page.FindByID(`n2`).Text()).To(Equal(second))
	Expect(page.FindByID(`result`).Text()).To(Equal(total))
}

func testError(page *agouti.Page, url string, expected string) {

	//fmt.Println("usageMsg :\n[" + usageMsg + "]")
	Expect(page.Navigate(url)).To(Succeed())
	time.Sleep(2 * time.Second)
	Eventually(page).Should(HaveURL(url))

	body, _ := page.Find("body").Text()
	//body := strings(page.Find("body").Text())
	body = strings.Replace(body, " ", "", -1)
	//fmt.Println("Website [" + body + "]")
	Expect(body).To(Equal(expected))

}
