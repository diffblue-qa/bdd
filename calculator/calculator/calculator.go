package calculator

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

func StartServer(port string) *http.Server {
	fmt.Printf("http://localhost:%s\n", port)
	srv := &http.Server{Addr: ":" + port}

	http.HandleFunc("/", CalculatorHandler) // each request calls handler

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			// cannot panic, because this probably is an intentional close
			log.Printf("Httpserver: ListenAndServe() error: %s", err)
		}
	}()

	return srv
}

func StopApp(srv http.Server) {
	log.Printf("main: stopping HTTP server")

	if err := srv.Shutdown(nil); err != nil {
		panic(err)
	}

	log.Printf("main: done. exiting")
}

type Page struct {
	PageTitle string
	N1        string
	Symbol    string
	N2        string
	Equals    string
	Result    string
	Error     string
}

func CalculatorHandler(w http.ResponseWriter, request *http.Request) {

	var cmd, first, second, symbol, result, errorMsg string
	var err1, err2 error
	var n1, n2 int
	equals := "="

	tmpl, err := template.ParseFiles("page.html")
	usage, _ := ioutil.ReadFile("usage.txt")

	fmt.Println(err)

	regex, _ := regexp.Compile("^/add|subtract|multiply|divide/[0-9]+/[0-9]+$")

	fmt.Println(request.URL.Path)

	if regex.MatchString(request.URL.Path) {

		s := strings.Split(request.URL.Path, "/")

		if len(s) == 4 {
			//fmt.Printf("s1=%s  s2=%s s3=%s ", s[1], s[2], s[3])
			cmd = s[1]
			n1, err1 = strconv.Atoi(s[2])
			n2, err2 = strconv.Atoi(s[3])
			first = s[2]
			second = s[3]
		}

		if err1 != nil || err2 != nil {
			errorMsg = string(usage)
			first = ""
			second = ""
			equals = ""
			result = ""
		} else {
			if cmd == "add" {
				result = strconv.Itoa(n1 + n2)
				symbol = "+"
			} else if cmd == "subtract" {
				result = strconv.Itoa(n1 - n2)
				symbol = "-"
			} else if cmd == "multiply" {
				result = strconv.Itoa(n1 * n2)
				symbol = "*"
			} else if cmd == "divide" {
				result = strconv.Itoa(n1 / n2)
				symbol = "/"
			} else {
				errorMsg = string(usage)
				first = ""
				second = ""
				equals = ""
			}
		}

	} else {
		errorMsg = string(usage)
		result = ""
		equals = ""
	}

	data := Page{"Calculator", first, symbol, second, equals, result, errorMsg}
	tmpl.Execute(w, data)
}
