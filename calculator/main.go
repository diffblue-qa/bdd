package main

import (
	"bdd/calculator/calculator"
	"log"
	"time"
)

func main() {

	port := "8000"
	srv := calculator.StartServer(port)

	log.Printf("main: serving for 10 seconds")

	time.Sleep(60 * time.Second)

	log.Printf("main: stopping HTTP server")

	// now close the server gracefully ("shutdown")
	// timeout could be given instead of nil as a https://golang.org/pkg/context/
	if err := srv.Shutdown(nil); err != nil {
		panic(err) // failure/timeout shutting down the server gracefully
	}

	log.Printf("main: done. exiting")
}
