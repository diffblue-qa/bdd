package main_test

import (
	"fmt"
	"image/png"
	"log"

	"os"
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/sclevine/agouti"
	//. "github.com/sclevine/agouti/matchers"
	//"github.com/sclevine/agouti/internal/mocks"
)

var _ = Describe("Calculator", func() {
	var (
		page *agouti.Page
		//session *mocks.Session
	)

	baseUrl := "http://localhost:8000/"
	delay := 0

	BeforeEach(func() {
		var err error
		//session = &mocks.Session{}
		page, err = agoutiDriver.NewPage()
		Expect(err).NotTo(HaveOccurred())
	})

	AfterEach(func() {
		Expect(page.Destroy()).To(Succeed())
	})

	passTable := []struct {
		context string
		cmd     string
		symbol  string
		first   string
		second  string
		total   string
	}{
		{"should add two integers", "add", "+", "55", "32", "87"},
		{"should subtract integers", "subtract", "-", "54", "32", "22"},
		{"should subtract integers", "subtract", "-", "-32", "10", "22"},
		{"should multiply two integers", "multiply", "*", "5", "3", "15"},
		{"should multiply two integers", "multiply", "*", "5", "1", "15"},
		{"should IntegerDivide two integers", "divide", "/", "9", "4", "2"},
		{"should IntegerDivide two integers", "divide", "/", "3", "4", "1"},
	}

	for _, row := range passTable {
		row := row

		It(row.context, func() {
			testPassFixture(page, row.cmd, row.symbol, row.first, row.second, row.total, delay)
		})
	}

	usageMsg := `Calculator
USAGE:
/add/x/y
/subtract/x/y
/multiply/x/y
/divide/x/y
wherexandyareintegers

`
	errorTable := []struct {
		desc string
		url  string
	}{
		{"should display usage information when no parameters are entered", ""},
		{"should display usage information when invalid command is entered", "adds/3/4"},
		{"should display usage information when missing first operand", "adds//4"},
		{"should display usage information when missing second operand", "adds/23/"},
		{"should display usage information when missing both operands", "add//"},
		{"should display usage information when missing second operand", "subtract/2/5"},
	}

	for _, row := range errorTable {
		row := row

		It(row.desc, func() {
			testErrorFixture(page, baseUrl+row.url, usageMsg)
		})
	}
})

func testPassFixture(page *agouti.Page, cmd string, symbol string, first string, second string, total string, delay int) {

	baseUrl := "http://localhost:8000/"
	url := baseUrl + cmd + "/" + first + "/" + second
	log.Println(url)

	Expect(page.Navigate(url)).To(Succeed())
	if delay > 0 {
		time.Sleep(1 * time.Second)
	}

	failures := InterceptGomegaFailures(func() {
		Expect(page.FindByID(`n1`).Text()).To(Equal(first))
		Expect(page.FindByID(`symbol`).Text()).To(Equal(symbol))
		Expect(page.FindByID(`n2`).Text()).To(Equal(second))
		Expect(page.FindByID(`result`).Text()).To(Equal(total))
	})
	//	for i, value := range failures {
	//		fmt.Printf("Error |%d]  %s \n", i, value)
	//	}
	//fmt.Printf("FailureCount  [%d]  [%s] \n", len(failures), failures[0])
	if len(failures) > 0 {
		takeScreenshot(page, url+".png")
		Expect(page.FindByID(`result`).Text()).To(Equal(total))
	}
}

func testErrorFixture(page *agouti.Page, url string, expected string) {

	Expect(page.Navigate(url)).To(Succeed())
	//time.Sleep(1 * time.Second)
	//Eventually(page).Should(HaveURL(url))

	body, _ := page.Find("body").Text()
	body = strings.Replace(body, " ", "", -1)
	failures := InterceptGomegaFailures(func() {
		Expect(body).To(Equal(expected))
	})

	if len(failures) > 0 {
		takeScreenshot(page, url+".png")
		Expect(body).To(Equal(expected))
	}

}

func takeScreenshot(page *agouti.Page, name string) {
	name = strings.Replace(name, "/", "_", -1)
	name = strings.Replace(name, "__", "_", -1)
	name = "screenshots/" + strings.Replace(name, ":", "", -1)
	fmt.Println("**** Screenshot : " + name)
	screenshot := page.Screenshot(name)
	fmt.Printf("Result of screenshot %s \n", screenshot)
	Expect(screenshot).To(Succeed())
	//defer os.Remove(name)
	file, _ := os.Open(name)
	_, err := png.Decode(file)
	Expect(err).NotTo(HaveOccurred())
}
