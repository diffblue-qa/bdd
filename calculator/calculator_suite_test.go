package main_test

import (
	"bdd/calculator/calculator"
	"os/exec"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/sclevine/agouti"

	"testing"
)

func TestCalculator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Calculator Suite")
}

var agoutiDriver *agouti.WebDriver

var _ = BeforeSuite(func() {
	exec.Command("killPort.sh")
	time.Sleep(5 * time.Second)

	go calculator.StartServer("8000")

	// Choose a WebDriver:

	//agoutiDriver = agouti.PhantomJS()
	agoutiDriver = agouti.ChromeDriver()

	Expect(agoutiDriver.Start()).To(Succeed())

})

var _ = AfterSuite(func() {
	Expect(agoutiDriver.Stop()).To(Succeed())
})
